---
layout: markdown_page
title: "Business Development"
---
BDR Handbook

[Up one level to the Demand Generation Handbook](/handbook/marketing/demand-generation/)    

## On this page
* [BDR FAQ's](#FAQ)  



## BDR FAQ's<a name="FAQ"></a>  


BDRs are frequently asked the following questions in some form or another:  
- I didn’t get a confirmation email, what should I do?  
- I need to change my password, or my password change didn’t work, what should I do?  
- I want to use [username] but it says it’s already taken, even though that user has never actually done anything on his/her account. Can you remove that user so I can get the username? 
- I made a mistake when setting up my account and need to change it. What should I do?
- GitLab.com seems slow. Why is that? Is anything being done to address this?
- I'm migrating from GitHub, BitBucket, SVN, etc. Do you have any migration tools?
- I'm looking to integrate with a Kanban board or project management tool. What do you recommend? What integrates? Is GitLab's project management functionality any good? Do you have any resources that address project management within GitLab?
- I really need [specific feature, integration, etc.]. Is that planned?
- I don't think [Feature X] works like it should. I think it should work [this way]. Can you fix it?
- What tools do you integrate with? Do you integrate with [Tool X]?
- How do groups work within GitLab? How do permissions work within groups? How many users can be in a group?
- Do read-only users still count toward our total user count for EE? What about bots?
- What are the different levels of permission within a project? Can I limit access for certain projects to specific users?
- Can I allow specific people to access my private projects, while keeping the project private?
- Does GitLab have git LFS or git annex functionality?
- I’m working with PostgreSQL and/or PostGIS. Is this compatible with GitLab? How does GitLab work with these?
- How many repositories can I have on my GitLab account?
- Are you really free? Will you really be free forever?
- How much does it cost for XX enterprise licenses? Do you send an invoice or can I pay by credit card?
- Do you provide any volume discounts?
- Do you provide any reseller discounts?
- There are some EE features we would like to use, but not all of them - can we pay per feature and add them to CE?
- How is GitLab.com backed up and secured?