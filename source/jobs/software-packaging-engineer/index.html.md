---
layout: job_page
title: "Software Packaging Engineer"
---

## Responsibilities

* As a Software Packaging engineer your main focus will be on [omnibus-gitlab][], the software that enables our users to install, upgrade, and use GitLab easily
* Work on improving various aspects of the omnibus-gitlab package (size, speed, reliability)
* Implement and bundle new services into the omnibus-gitlab package, for more details see our [direction scope][]
* Work with the rest of the GitLab development team in supporting newly created features and resolving bugs on the omnibus-gitlab project side
* Work on making GitLab easier to install and configure for all users (through Chef cookbooks, Puppet modules, Ansible and Salt scripts)
* Improve package build processes
* Help community packaging projects (eg. native Debian, native Fedora and native Arch packages)

## Workflow

* In cooperation with the Packaging team, issues to work on will be sorted per release cycle.
* Deciding priority of the issues can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/#prioritize).

## Requirements for Applicants

* Linux experience, comfortable between Debian and RHEL based systems
* Chef experience
* Programming experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
* Knowledge of container ecosystems (Docker, rkt, etc.)
* Packaging experience is an advantage, not a requirement. However, you are
  expected to have basic knowledge of at least .deb and .rpm package archives.
* Collaborative team spirit with good communication skills
* You share our [values](/handbook/#values), and work in accordance with those values

[omnibus-gitlab]: https://gitlab.com/gitlab-org/omnibus-gitlab
[direction scope]: https://about.gitlab.com/direction/#scope-a-namescopea
